//  Copyright (c) 2014-2018 Glance Networks, Inc. All rights reserved.
//
//  GlanceFramework.h
//
//

#import <Foundation/Foundation.h>
#import "VideoEncoder.h"
#import "GlanceVideoSession.h"
#import "GlanceVideoSessionState.h"

// Events -----------------------------------------------------------------------------------------

typedef enum EventCode : NSUInteger
{
    EventNone
    ,EventInvalidParameter      ///< An invalid input parameter was passed into an API method
    ,EventInvalidState          ///< There was an attempt to invoke an Action that was not valid in current state
    ,EventLoginSucceeded        ///< User login succeeded
    ,EventLoginFailed           ///< User login failed
    ,EventPrivilegeViolation    ///< There was an attempt to do something that is not permitted by the User's privileges
    ,EventInvalidWebserver      ///< Attempt to direct the client to connect to a webserver that the client has not been configured to allow
    ,EventUpgradeAvailable      ///< An upgrade to the client library is available
    ,EventUpgradeRequired       ///< An upgrade to the client library is required
    ,EventCompositionDisabled   ///< Desktop composition was disabled (Windows Vista only)
    ,EventConnectedToSession    ///< Client successfully connected to a session, or reconnected after changing roles (display to viewer or vice versa)
    ,EventSwitchingToView       ///< A viewer participant in the session has started showing his screen, so the display is switching to view
    ,EventStartSessionFailed    ///< An attempt to start a new session was unsuccessful
    ,EventJoinFailed            ///< An attempt to join a session was unsuccessful
    ,EventSessionEnded          ///< Session endded
    ,EventFirstGuestSession     ///< A guest's first-ever session just ended, prompt user to uninstall
    ,EventTunneling             ///< Connection is tunneling
    ,EventRestartRequired       ///< To complete the driver install, a machine restart is required
    ,EventConnectionWarning     ///< Connectivity has been lost
    ,EventClearWarning          ///< Connectivity has been restored
    ,EventGuestCountChange      ///< The guest count changed
    ,EventViewerClose           ///< User clicked the viewer window close button
    ,EventActionsChange         ///< The set of allowed actions changed (NOT IMPLEMENTED)
    ,EventDriverInstallError    ///< There was an error installing Glance SpeedBoost.
    ,EventRCDisabled            ///< Remote control has been disabled.
    ,EventDeviceDisconnected    ///< Captured (video) device disconnected.  Can be reconnected and session continued.
    ,EventDeviceReconnected     ///< Captured (video) device reconnected.
    ,EventGesture               ///< A gesture was received.
    ,EventException             ///< Unexpected exception
    ,EventScreenshareInvitation ///< A screenshare session invitation received from another session participant
    ,EventMessageReceived       ///< A user message was received from another session participant
    ,EventChildSessionStarted   ///< A child session started.
    ,EventChildSessionEnded     ///< A child session ended.
    ,EventJoinChildSessionFailed///< An invitation was received to join a child session was received but the session could not be found
    ,EventVisitorInitialized    ///< Visitor API successfully initialized
} EventCode;

typedef enum GlanceEventType : NSUInteger
{
    EventTypeNone,
    EventInfo,
    EventWarning,
    EventError,
    EventAssertFail
} GlanceEventType;

@interface GlanceEvent: NSObject
{
    EventCode           code;
    GlanceEventType           type;
    NSString        *   message;
    NSDictionary    *   properties;
}
@property (readonly) EventCode           code;
@property (readonly) GlanceEventType     type;
@property (readonly) NSString        *   message;
@property (readonly) NSDictionary    *   properties;

- (NSString *)getProperty:(NSString *)key;
@end

// Event Properties

// EventSessionEnded
// "reason"
typedef enum GlanceEventSessionEndedReason : NSUInteger
{
    SessionEndReasonNone,
    SessionEndReasonThisSide,           // Session ended intentionally on this side
    SessionEndReasonOtherSide,          // Session ended by other side
    SessionEndReasonCantConnect,        // Unable to connect to the session server
    SessionEndReasonConnDrop,           // Connection dropped and could not be reestablished
    SessionEndReasonKilled,             // Session was killed
    SessionEndReasonHasDisplay,         // Two guests tried to join reverse session simultaneously
    SessionEndReasonDeclined,           // Guest in a reverse session declined
    SessionEndTimedout,                 // Timed out waiting for guest to join
    SessionEndReplaced,                 // A new viewer was open by the same guest, replacing this one
    SessionEndReasonError,              // An unexpected error occurred.
} GlanceEventSessionEndedReason;

@interface GlanceBase : NSObject
{
    id      evtTarget;
    SEL     evtSelector;
}

- (void)onEventNotifyTarget:(id)target selector:(SEL)selector;

@end


// User Settings and Privileges -------------------------------------------------------------------

@interface GlanceSettings : GlanceBase
{
    void * gs;
}
- (id)init;
- (void)dealloc;

- (void)set:(NSString *)name value:(NSString *)value;
- (NSString *)get:(NSString *)name;

@end

// Settings
extern NSString * const kGlanceSettingGlanceServer;
extern NSString * const kGlanceSettingGlanceAddress;
extern NSString * const kGlanceSettingPassword;
extern NSString * const kGlanceSettingSessionKey;
extern NSString * const kGlanceSettingForceAJAX;
extern NSString * const kGlanceSettingEncrypt;
extern NSString * const kGlanceSettingEnableShowback;
extern NSString * const kGlanceSettingGIFlags;
extern NSString * const kGlanceSettingEnableAgentVideo;

// User and Group level privileges
extern NSString * const kGlancePrivRCAllowed;
extern NSString * const kGlancePrivReverseAllowed;
extern NSString * const kGlancePrivShowbackAllowed;
extern NSString * const kGlancePrivSetGIFlagsAllowed;
extern NSString * const kGlancePrivKeylessAllowed;
extern NSString * const kGlancePrivVideoAllowed;
extern NSString * const kGlancePrivForceEncryption;
extern NSString * const kGlancePrivNumGuestsAllowed;

// User Subscription attributes
extern NSString * const kGlanceSubIsTrial;
extern NSString * const kGlanceSubPlanTitle;
extern NSString * const kGlanceSubPlanTimeout;


// User  ------------------------------------------------------------------------------------------

@interface GlanceCredentials : NSObject
{
@public
    NSString *  username;
    int         partnerId;
    NSString *  partnerUserId;
    NSString *  loginkey;
    NSString *  gssnid;
}
@property (retain) NSString * username;
@property int partnerId;
@property (retain) NSString * partnerUserId;
@property (retain) NSString * loginkey;
@property (retain) NSString * gssnid;

- (id)  init;
- (void)dealloc;

@end

@interface GlanceUser : GlanceBase
{
    void * user;
}

- (id)  init;
- (void)dealloc;

- (void)                authenticate;
- (void)                authenticateWithUsername:(NSString *)username password:(NSString *)password;
- (void)                authenticateWithUsername:(NSString *)username key:(NSString *)loginkey;
- (void)                authenticateWithPartnerId:(int)partnerId userId:(NSString *)partnerUserId key:(NSString *)loginkey;
- (bool)                authenticated;
- (NSString *)          getGssnidValidForSeconds:(int)requiredTime error:(NSString **)msg;
- (GlanceCredentials *) getCredentialsValidForSeconds:(int)requiredTime error:(NSString **)msg;
- (NSString *)          getAccountSetting:(NSString *)name;
- (void)                logout;

@end

// Session Parameters and Info --------------------------------------------------------------------

@interface GlanceDisplayParams : NSObject
{
@public
    NSString *              displayName;    ///< Display name or number, or video device name.  "Main" or blank indicates the main display
    float                   scale;          ///< Video device capture scaling factor
    int                     captureWidth;   ///< Video device capture width (0 for default size)
    int                     captureHeight;  ///< Video device capture height (0 for default size)
    bool                    video;          ///< Video mode, for video device capture only
    long                    window;         ///< Show window with this window number, pass "Window" in displayName
    long                    process;        ///< Show application with this process id, pass "Process" in displayName
    NSString *              names;          ///< Not yet used on Mac
}
@property float scale;
@property int captureWidth;
@property int captureHeight;
@property (retain) NSString * displayName;
@property long window;
@property long process;
@property (retain) NSString * names;

- (id)init;
- (instancetype)initWithDisplayName:(NSString*)displayName captureWidth:(int)captureWidth captureHeight:(int)captureHeight;
- (void)dealloc;
@end


typedef enum GuestInfoFlags : UInt32
{
    GuestNameRequired  = 0x0200,
    GuestEmailRequired = 0x0400,
    GuestPhoneRequired = 0x0800,
    GuestNameHidden    = 0x1000,
    GuestEmailHidden   = 0x2000,
    GuestPhoneHidden   = 0x4000
} GuestInfoFlags;


@interface GlanceStartParams : NSObject
{
@public
    unsigned long           mainCallId;         ///< The call id of the main session if this is a subsession
    short                   maxGuests;          ///< Guest count limit
    bool                    show;               ///< True if starting a session to show the screen, false for view
    unsigned long           guestInfoFlags;     ///< Flags indicating what guest information is collected and/or required
    bool                    encrypt;            ///< true if the session (both guest and host side) should be encrypted, false otherwise
    NSString *              key;                ///< The session key
    GlanceDisplayParams *   displayParams;      ///< Parameters that specify the display to be shown when the session starts
    bool                    requestRC;          ///< Set to True if the host wants remote control in a reverse (view) session
    bool                    instantJoin;        ///< Set to True if guests should join with the Instant viewer (obsolete)
    bool                    forceTunnel;        ///< Use a tunneling protocol
    bool                    viewerCloseable;    ///< true if the user should be able to close the viewer window directly, false to get an EventViewerClose message instead
    bool                    reportErrors;       ///< true if the library should report errors to Glance
    bool                    persist;            ///<  Session should persist until host ends (or timeout, similar to waiting)
}
@property           unsigned long           mainCallId;
@property           short                   maxGuests;
@property           bool                    show;
@property           unsigned long           guestInfoFlags;
@property           bool                    encrypt;
@property (retain)  NSString *              key;
@property (retain)  GlanceDisplayParams *   displayParams;
@property           bool                    requestRC;
@property           bool                    instantJoin;
@property           bool                    forceTunnel;
@property           bool                    viewerCloseable;
@property           bool                    reportErrors;
@property           bool                    persist;

- (id)init;
- (void)dealloc;
@end


@interface GlanceJoinParams : NSObject
{
@public
    unsigned long           guestID;
    bool                    decline;        // join only for the purpose of notifying host of decline
    NSString *              guestName;
    NSString *              guestPhone;
    NSString *              guestEmail;
    bool                    forceTunnel;
    bool                    viewerCloseable;
}
@property unsigned long         guestID;
@property bool                  decline;
@property (retain) NSString *   guestName;
@property (retain) NSString *   guestPhone;
@property (retain) NSString *   guestEmail;
@property bool                  forceTunnel;
@property bool                  viewerCloseable;

- (id)init;
- (void)dealloc;
@end


@interface GlanceSessionInfo : NSObject
{
@public
    unsigned long   callId;
    int             nGuests;
    NSString *      glanceAddress;
    NSString *      key;
    NSString *      hostName;
    bool            rcRequested;
    bool            isGuest;
    bool            isReverse;
    bool            rcEnabled;
    bool            sbEnabled;
    bool            isPaused;
    bool            isConnected;
    bool            isShowing;
    bool            isViewing;
    NSString *      displayName;
}
@property (readonly) unsigned long   callId;
@property (readonly) int             nGuests;
@property (readonly) NSString *      glanceAddress;
@property (readonly) NSString *      key;
@property (readonly) NSString *      hostName;
@property (readonly) bool            rcRequested;
@property (readonly) bool            isGuest;
@property (readonly) bool            isReverse;
@property (readonly) bool            rcEnabled;
@property (readonly) bool            sbEnabled;
@property (readonly) bool            isPaused;
@property (readonly) bool            isConnected;
@property (readonly) bool            isShowing;
@property (readonly) bool            isViewing;
@property (readonly) NSString *      displayName;
@end


typedef enum GlanceAction : NSUInteger
{
    ActionNone
    ,ActionStartShow                ///< Start a session to show screen
    ,ActionStartView                ///< Start a session to view screen
    ,ActionShowDisplay              ///< Show a different screen or device
    ,ActionPause                    ///< Pause a session which is currently showing.
    ,ActionUnpause                  ///< Unpause a session
    ,ActionEnableRemoteControl      ///< Enable remote control, allowing other side to control screen
    ,ActionDisableRemoteControl     ///< Disable remote control
    ,ActionEnableShowback           ///< Enable showback for the session, allowing guests to show their screens
    ,ActionDisableShowback          ///< Disable showback
    ,ActionEnableGestures           ///< Enable gesturing for the session, allowing guests to draw on the display side screen
    ,ActionDisableGestures          ///< Disable gesturing
    ,ActionEnd                      ///< End the session
} GlanceAction;

typedef enum GlanceDisplayType :NSUInteger { DisplayTypeMonitor, DisplayTypeDevice } GlanceDisplayType;

// Sessions ---------------------------------------------------------------------------------------

@interface GlanceSession : GlanceBase
{
}

- (id)      init;
- (void)    dealloc;

// Display information
+ (int)                 getDisplayCount;         // total number of currently connected monitors and devices
+ (int)                 getMainMonitor;
+ (void)                identifyMonitors;        // draws a number on each monitor (not implemented on Mac)
+ (GlanceDisplayType)   getTypeOfDisplay:(int)n;
+ (NSString *)          getNameOfDisplay:(int)n;

- (bool)                canInvokeAction:(GlanceAction)action;
- (void)                invokeAction:(GlanceAction)action;
- (GlanceSessionInfo *) getSessionInfo;
- (void)                sendUserMessage:(NSString *)message;
- (void)                sendScreenshareInvitation:(NSString *)username key:(NSString *)sessionKey viewName:(NSString *)screenshareView;

- (void)                end;

- (void)                showDisplay:(GlanceDisplayParams *)dParams;
- (void)                setMaskedViews:(NSSet *)views;
@end


@interface GlanceHostSession : GlanceSession
{
    void * hs;
}

- (id)  initWithUser:(GlanceUser *)u;
- (void)dealloc;

- (void)                show:(GlanceStartParams *)sp;
- (void)                view:(GlanceStartParams *)sp;
@end


@interface GlanceGuestSession : GlanceSession
{
    void * gs;
}

- (id)init;
- (id)initWithUser:(GlanceUser *)u;
- (void)dealloc;

- (GlanceSessionInfo *) lookupWithUsername:(NSString *)username key:(NSString*)key;
- (void)                joinWithUsername:(NSString *)username key:(NSString*)key params:(GlanceJoinParams *)jp;
- (void)                joinWithGroupId: (int)groupid key:(NSString*)key params:(GlanceJoinParams *)jp;
@end

/**
 * GlanceVisitorDelegate
 *
 * A delegate for the GlanceVisitor interface to recieve events like session start, session ended and failures.
 */
@protocol GlanceVisitorDelegate
-(void)glanceVisitorEvent:(GlanceEvent*)event;
@end

/**
 * GlanceVisitor
 *
 * A utility class to interact with the Glance Visitor SDK.
 * There can only be one Visitor session at a time with this SDK.
 */
@interface GlanceVisitor : NSObject

/**
 * Call this once to initialize the SDK. You must pass your groupID obtained from Glance.  The token parameter is reserved for future use, pass an empty string now.  You may optionally pass the visitor’s name, email or phone or any strings useful to you.  These will be saved in the Glance database and you may obtain these session attributes via reports or API calls.
 *
 * **This method should only be called once.**
 *
 * @param groupId   Glance provided group ID integer associated with your account. _required_
 * @param token     Glance provided token string. _optional_
 * @param name      Name _optional_ Maximum length is 63 characters
 * @param email     Email address _optional_ Maximum length is 127 characters
 * @param phone     Phone number _optional_ Maximum length is 31 characters
 */
+(void)init:(int)groupId token:(NSString*)token name:(NSString*)name email:(NSString*)email phone:(NSString*)phone;

/**
 * The method initVistor is the same as init, it can be used from Swift 5 where "init" is reserved.
 Call initVisitor once to initialize the SDK. You must pass your groupID obtained from Glance.  The token parameter is reserved for future use, pass an empty string now.  You may optionally pass the visitor’s name, email or phone or any strings useful to you.  These will be saved in the Glance database and you may obtain these session attributes via reports or API calls.
 *
 * **This method should only be called once.**
 *
 * @param groupId   Glance provided group ID integer associated with your account. _required_
 * @param token     Glance provided token string. _optional_
 * @param name      Name _optional_ Maximum length is 63 characters
 * @param email     Email address _optional_ Maximum length is 127 characters
 * @param phone     Phone number _optional_ Maximum length is 31 characters
 */
+(void)initVisitor:(int)groupId token:(NSString*)token name:(NSString*)name email:(NSString*)email phone:(NSString*)phone;

/**
 * Start a screensharing session to show the current application, using a random numeric key.  Note that for Visitor sessions, the key identifies the session but is not sufficient to join the session.  Agents joining sessions must be logged-in or otherwise authenticated to Glance and must be members of the group specified in the call to init.  After calling startSession, the delegate will receive either EventSessionStarted or EventStartSessionFailed.
 *
 * **init: should be called first.**
 */
+(void)startSession;

/**
 * Start a screensharing session with a key you provide.  The key may only contain characters from the Base64 URL set.  Maximum length is 63.
 *
 * **init: must be called first.**
 *
 * @param sessionKey     Session key (4 digit string) _required_
 */
+(void)startSession:(NSString*)sessionKey;

/**
 * Start a screensharing session with additional parameters. See GlanceStartParams.
 *
 * **init: must be called first.**
 *
 * @param sp     Start parameters _required_
 */
+(void)startSessionWithParams:(GlanceStartParams *)sp;

/**
 * Return information about the current session.
 */
+(GlanceSessionInfo *)getSessionInfo;

/**
  * Pause or unpause the screenshare session
  * While a session is paused, the viewer displays a customizable message instead of the application.
  *
  * @param pause     Pause session if true or unpause if false _required_
  */
+(void)pause:(BOOL)pause;

/**
  * Enable remote control, so that the agent can control the visitor's application.
  * Remote control must be enabled for the account.
  *
  * @param enable     True to enable remote control or false to disable it
  */
+(void)enableRC:(BOOL)enable;

/**
  * Shows the specified display.
  *
  * @param displayParams    Parameters indicating which display to show, and how to show it.
  */
+(void)showDisplay:(GlanceDisplayParams *)displayParams;

/**
 * End the screensharing session.  Calling endSession initiates the process of ending the session. The session is not actually ended until the EventSessionEnded event is fired.
 */
+(void)endSession;

/**
 * Adds a delegate for the Glance visitor session to receive events.
 *
 * @param delegate     GlanceVisitorDelegate instance _required_
 */
+(void)addDelegate:(id<GlanceVisitorDelegate>) delegate;

/**
 * Removes a delegate for the Glance visitor session to receive events.
 *
 * @param delegate     GlanceVisitorDelegate instance _required_
 */
+(void)removeDelegate:(id<GlanceVisitorDelegate>) delegate;

/**
 * Sets the custom viewer delegate to provide a custom implementation of the agent video viewer.
 *
 * @param customViewerDelegate     GlanceCustomViewerDelegate instance _required_
 */
// +(void)setCustomViewerDelegate:(id<GlanceCustomViewerDelegate>) customViewerDelegate;

/**
 * Sets the list of UIViews to mask during the Glance visitor session.
 *
 * @param views  NSSet of NSView instances to mask _required_
 */
+(void)setMaskedViews:(NSSet *)views;

/**
 * Adds a UIView to mask during the Glance visitor session.
 *
 * @param view  NSView instance to mask _required_
 */
//+(void)addMaskedView:(NSView*)view;

/**
 * Remove a UIView from being masked during a Glance visitor session.
 *
 * @param view  NSView instance to mask _required_
 */
//+(void)removeMaskedView:(NSView*)view;

@end
