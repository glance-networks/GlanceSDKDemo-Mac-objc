//
//  AppDelegate.m
//  GlanceSDKDemo-Mac-objc
//
//  Created by Ed Hardebeck on 11/2/18.
//  Copyright © 2018 Glance Networks, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

const int GLANCE_GROUP_ID = 15687;      // Replace this with your own Group ID from Glance

@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSButton *startButton;
@property (weak) IBOutlet NSButton *endButton;
@property (weak) IBOutlet NSTextField *keyText;
@property (weak) IBOutlet NSTextField *statusText;
@property (weak) IBOutlet NSTextField *maskedView;
@property (weak) IBOutlet NSPopUpButton *showMenu;
@property BOOL inSession;
@end

@implementation AppDelegate

@synthesize keyText;
@synthesize statusText;
@synthesize maskedView;
@synthesize inSession;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Configure Glance Visitor SDK
    [GlanceVisitor addDelegate: self];
    [GlanceVisitor init:GLANCE_GROUP_ID token:@"" name:@"" email:@"" phone:@""];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication {
    return YES;
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
    [GlanceVisitor endSession];
}

-(void)glanceVisitorEvent:(GlanceEvent*)event{
    switch (event.code)
    {
        case EventConnectedToSession: {
            // Successfully connected to server
            // Get generated session key
            NSString * sessionKey = event.properties[@"sessionkey"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.keyText.stringValue = sessionKey;
                self.statusText.stringValue = @"Session started - give the key above to the agent";
                self.startButton.hidden = YES;
                self.endButton.hidden = NO;
            });
            break;
        }
        
        case EventGuestCountChange: {
            // agent connected, session now active
            dispatch_async(dispatch_get_main_queue(), ^{
                self.statusText.stringValue = @"Agent connected";
            });
            break;
        }
        
        case EventStartSessionFailed: {
            // Couldn't start session
            // Error message can be found in event.message
            NSLog(@"Could not start session: %@", event.message);
            dispatch_async(dispatch_get_main_queue(), ^{
                self.statusText.stringValue = [@"Could not start session:" stringByAppendingString: event.message];
            });
            break;
        }
        
        case EventSessionEnded: {
            // Session ended
            dispatch_async(dispatch_get_main_queue(), ^{
                self.statusText.stringValue = @"Session ended";
                self.keyText.stringValue = @"";
                
                self.startButton.hidden = NO;
                self.endButton.hidden = YES;
            });
            break;
        }
        
        default: {
            // It is recommended that you at log all other events to help debug during development
            // Best practice is to log all other events of type EventAssertFail, EventError, or EventWarning
            if (event.type == EventWarning || event.type == EventError || event.type == EventAssertFail) {
                NSLog(@"Glance Visitor Event: %lu, %@", (unsigned long)event.code, event.message);
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.statusText.stringValue = [NSString stringWithFormat: @"Event: %lu, %@", event.code, event.message];
                });
            }
            break;
        }
    }
}


- (IBAction)startSession:(id)sender {
    switch ([self.showMenu selectedTag])
    {
        case 0: {    // This App
            [GlanceVisitor startSession];
            break;
        }
        
        case 1: {   // Main Display
            GlanceStartParams * startParams = [[GlanceStartParams alloc] init];
            startParams->displayParams->displayName = @"Main";
            startParams->key = @"GLANCE_KEYTYPE_RANDOM";
            [GlanceVisitor startSessionWithParams: startParams];
            break;
        }
            
        default:
            NSLog(@"Unexpected Show menu selection");
            return;
    }
    
    [GlanceVisitor setMaskedViews: [NSSet setWithObjects: keyText, maskedView, nil]];
    self.inSession = YES;
}

- (IBAction)endSession:(id)sender {
    self.inSession = NO;
    [GlanceVisitor endSession];
}

- (IBAction)selectShownItem:(id)sender {
    if (!self.inSession) return;
    
    GlanceDisplayParams * dp = [[GlanceDisplayParams alloc] init];

    switch ([self.showMenu selectedTag])
    {
        case 0: {    // This App
            dp->displayName = @"Process";
            dp->process = [[NSRunningApplication currentApplication] processIdentifier];
            // to show another application: NSRunningApplication runningApplicationsWithBundleIdentifier: bid];
            break;
        }
        
        case 1: {   // Main Display
            dp->displayName = @"Main";
            break;
        }
    }
    
    [GlanceVisitor showDisplay: dp];
}

@end
